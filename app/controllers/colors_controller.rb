class ColorsController < ApplicationController
  def index
  end

  # returns a sampled color, using the most recent sample
  def sample
    s = Sample.last
    respond_to do |format|
      format.html # sample.html.erb
      format.json { render json: s }
    end
  end
end
