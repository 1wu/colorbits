require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

module Clockwork
  handler do |job|
    if (job == 'generate.random.sample') then
      Sample.generate_new
    end
  end

  every(3.seconds, 'generate.random.sample')
end
