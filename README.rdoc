== README

This code snippet implements the following steps

* Set up a queue (here a simple database), with a RGB tuple (0-255). A
  new tuple is inserted every 3 seconds using the 'clockwork' cron-like gem.

* Serve a simple web page with a start button. When clicked, the
  front-end fetches a tuple from the backend using AJAX and updates
  the CSS background color of the page, every three seconds.

Leslie Wu