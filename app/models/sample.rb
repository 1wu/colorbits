class Sample < ActiveRecord::Base
  def self.generate_new
    s = Sample.new
    s.background_color_r = rand(255+1)
    s.background_color_g = rand(255+1)
    s.background_color_b = rand(255+1)
    s.save
    s
  end
end
