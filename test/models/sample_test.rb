require 'test_helper'

class SampleTest < ActiveSupport::TestCase
  test "should_generate_valid_colors" do
    # TODO handle more sophisticated database testing
    500.times do
      s = Sample.generate_new
      [s.background_color_r, s.background_color_g, s.background_color_b].each do |c|
        assert c.is_a? Numeric
        assert c >= 0
        assert c <= 255
      end
    end
  end

end
