class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.integer :background_color_r
      t.integer :background_color_g
      t.integer :background_color_b

      t.timestamps
    end
  end
end
