require 'test_helper'

class ColorsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get sample" do
    get :sample
    assert_response :success
  end

  # end-to-end(ish) test: does new sample equate to what JSON endpoint returns?
  test "sample generation and JSON GET are same" do
    sample_new = Sample.generate_new
    get :sample, :format => 'json'
    assert_not_nil @response.body
    sample_json = JSON.parse(@response.body)
    assert_not_nil sample_json
    assert_equal sample_json.background_color_r, sample_new.background_color_r
    assert_equal sample_json.background_color_g, sample_new.background_color_g
    assert_equal sample_json.background_color_b, sample_new.background_color_b
    assert_response :success
  end
end
